package ru.pisarev.tm.command.system;

import com.jcabi.manifests.Manifests;
import ru.pisarev.tm.command.AbstractCommand;

public class AboutDisplayCommand extends AbstractCommand {
    @Override
    public String name() {
        return "about";
    }

    @Override
    public String arg() {
        return "-a";
    }

    @Override
    public String description() {
        return "Display developer info.";
    }

    @Override
    public void execute() {
        System.out.println(Manifests.read("developer"));
        System.out.println(Manifests.read("email"));
    }
}

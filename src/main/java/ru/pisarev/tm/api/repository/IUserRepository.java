package ru.pisarev.tm.api.repository;

import ru.pisarev.tm.api.IRepository;
import ru.pisarev.tm.model.User;

public interface IUserRepository extends IRepository<User> {

    User findByLogin(final String login);

    User findByEmail(final String email);

    User removeUserByLogin(final String login);
}

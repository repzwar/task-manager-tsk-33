package ru.pisarev.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.pisarev.tm.api.service.ITaskService;
import ru.pisarev.tm.api.service.ServiceLocator;
import ru.pisarev.tm.model.Session;
import ru.pisarev.tm.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@WebService
@NoArgsConstructor
public final class TaskEndpoint extends AbstractEndpoint {

    private ITaskService taskService;

    public TaskEndpoint(@NotNull final ServiceLocator serviceLocator, final ITaskService taskService) {
        super(serviceLocator);
        this.taskService = taskService;
    }

    @WebMethod
    public List<Task> TaskFindAll(@NotNull @WebParam(name = "session") final Session session) {
        serviceLocator.getSessionService().validate(session);
        return taskService.findAll(session.getUserId());
    }

    @WebMethod
    public void TaskAddAll(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "collection") final Collection<Task> collection
    ) {
        serviceLocator.getSessionService().validate(session);
        taskService.addAll(session.getUserId(), collection);
    }

    @WebMethod
    public Task TaskAdd(
            @WebParam(name = "session") final Session session, @WebParam(name = "task") final Task entity
    ) {
        serviceLocator.getSessionService().validate(session);
        return taskService.add(session.getUserId(), entity);
    }

    @WebMethod
    public Task TaskFindById(
            @WebParam(name = "session") final Session session, @WebParam(name = "id") final String id
    ) {
        serviceLocator.getSessionService().validate(session);
        return taskService.findById(session.getUserId(), id);
    }

    @WebMethod
    public void TaskClear(@WebParam(name = "session") final Session session) {
        serviceLocator.getSessionService().validate(session);
        taskService.clear(session.getUserId());
    }

    @WebMethod
    public Task TaskRemoveById(
            @WebParam(name = "session") final Session session, @WebParam(name = "id") final String id
    ) {
        serviceLocator.getSessionService().validate(session);
        return taskService.removeById(session.getUserId(), id);
    }

    @WebMethod
    public Task TaskRemove(
            @WebParam(name = "session") final Session session, @WebParam(name = "task") final Task entity
    ) {
        serviceLocator.getSessionService().validate(session);
        return taskService.remove(session.getUserId(), entity);
    }

    @WebMethod
    public Task TaskFindByName(
            @WebParam(name = "session") final Session session, @WebParam(name = "name") final String name
    ) {
        serviceLocator.getSessionService().validate(session);
        return taskService.findByName(session.getUserId(), name);
    }

    @WebMethod
    public Task TaskFindByIndex(
            @WebParam(name = "session") final Session session, @WebParam(name = "index") final Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        return taskService.findByIndex(session.getUserId(), index);
    }

    @WebMethod
    public Task TaskRemoveByName(
            @WebParam(name = "session") final Session session, @WebParam(name = "name") final String name
    ) {
        serviceLocator.getSessionService().validate(session);
        return taskService.removeByName(session.getUserId(), name);
    }

    @WebMethod
    public Task TaskRemoveByIndex(
            @WebParam(name = "session") final Session session, @WebParam(name = "index") final Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        return taskService.removeByIndex(session.getUserId(), index);
    }

    @WebMethod
    public Task TaskUpdateById(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "id") final String id,
            @WebParam(name = "name") final String name,
            @WebParam(name = "description") final String description
    ) {
        serviceLocator.getSessionService().validate(session);
        return taskService.updateById(session.getUserId(), id, name, description);
    }

    @WebMethod
    public Task TaskUpdateByIndex(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "index") final Integer index,
            @WebParam(name = "name") final String name,
            @WebParam(name = "description") final String description
    ) {
        serviceLocator.getSessionService().validate(session);
        return taskService.updateByIndex(session.getUserId(), index, name, description);
    }

    @WebMethod
    public Task TaskStartById(
            @WebParam(name = "session") final Session session, @WebParam(name = "id") final String id
    ) {
        serviceLocator.getSessionService().validate(session);
        return taskService.startById(session.getUserId(), id);
    }

    @WebMethod
    public Task TaskStartByIndex(
            @WebParam(name = "session") final Session session, @WebParam(name = "index") final Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        return taskService.startByIndex(session.getUserId(), index);
    }

    @WebMethod
    public Task TaskStartByName(
            @WebParam(name = "session") final Session session, @WebParam(name = "name") final String name
    ) {
        serviceLocator.getSessionService().validate(session);
        return taskService.startByName(session.getUserId(), name);
    }

    @WebMethod
    public Task TaskFinishById(
            @WebParam(name = "session") final Session session, @WebParam(name = "id") final String id
    ) {
        serviceLocator.getSessionService().validate(session);
        return taskService.finishById(session.getUserId(), id);
    }

    @WebMethod
    public Task TaskFinishByIndex(
            @WebParam(name = "session") final Session session, @WebParam(name = "index") final Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        return taskService.finishByIndex(session.getUserId(), index);
    }

    @WebMethod
    public Task TaskFinishByName(
            @WebParam(name = "session") final Session session, @WebParam(name = "name") final String name
    ) {
        serviceLocator.getSessionService().validate(session);
        return taskService.finishByName(session.getUserId(), name);
    }
}

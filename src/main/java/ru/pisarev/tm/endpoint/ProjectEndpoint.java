package ru.pisarev.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.pisarev.tm.api.service.IProjectService;
import ru.pisarev.tm.api.service.ServiceLocator;
import ru.pisarev.tm.model.Project;
import ru.pisarev.tm.model.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@WebService
@NoArgsConstructor
public final class ProjectEndpoint extends AbstractEndpoint {

    private IProjectService projectService;

    public ProjectEndpoint(
            @NotNull final ServiceLocator serviceLocator, @NotNull final IProjectService projectService
    ) {
        super(serviceLocator);
        this.projectService = projectService;
    }

    @WebMethod
    public List<Project> ProjectFindAll(@WebParam(name = "session") final Session session) {
        serviceLocator.getSessionService().validate(session);
        return projectService.findAll(session.getUserId());
    }

    @WebMethod
    public void ProjectAddAll(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "collection") final Collection<Project> collection
    ) {
        serviceLocator.getSessionService().validate(session);
        projectService.addAll(session.getUserId(), collection);
    }

    @WebMethod
    public Project ProjectAdd(
            @WebParam(name = "session") final Session session, @WebParam(name = "entity") final Project entity
    ) {
        serviceLocator.getSessionService().validate(session);
        return projectService.add(session.getUserId(), entity);
    }

    @WebMethod
    public Project ProjectFindById(
            @WebParam(name = "session") final Session session, @WebParam(name = "id") final String id
    ) {
        serviceLocator.getSessionService().validate(session);
        return projectService.findById(session.getUserId(), id);
    }

    @WebMethod
    public void ProjectClear(@WebParam(name = "session") final Session session) {
        serviceLocator.getSessionService().validate(session);
        projectService.clear(session.getUserId());
    }

    @WebMethod
    public Project ProjectRemoveById(
            @WebParam(name = "session") final Session session, @WebParam(name = "id") final String id
    ) {
        serviceLocator.getSessionService().validate(session);
        return projectService.removeById(session.getUserId(), id);
    }

    @WebMethod
    public Project ProjectRemove(
            @WebParam(name = "session") final Session session, @WebParam(name = "entity") final Project entity
    ) {
        serviceLocator.getSessionService().validate(session);
        return projectService.remove(session.getUserId(), entity);
    }

    @WebMethod
    public Project ProjectFindByName(
            @WebParam(name = "session") final Session session, @WebParam(name = "name") final String name
    ) {
        serviceLocator.getSessionService().validate(session);
        return projectService.findByName(session.getUserId(), name);
    }

    @WebMethod
    public Project ProjectFindByIndex(
            @WebParam(name = "session") final Session session, @WebParam(name = "index") final Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        return projectService.findByIndex(session.getUserId(), index);
    }

    @WebMethod
    public Project ProjectRemoveByName(
            @WebParam(name = "session") final Session session, @WebParam(name = "name") final String name
    ) {
        serviceLocator.getSessionService().validate(session);
        return projectService.removeByName(session.getUserId(), name);
    }

    @WebMethod
    public Project ProjectRemoveByIndex(
            @WebParam(name = "session") final Session session, @WebParam(name = "index") final Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        return projectService.removeByIndex(session.getUserId(), index);
    }

    @WebMethod
    public Project ProjectUpdateById(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "id") final String id,
            @WebParam(name = "name") final String name,
            @WebParam(name = "description") final String description
    ) {
        serviceLocator.getSessionService().validate(session);
        return projectService.updateById(session.getUserId(), id, name, description);
    }

    @WebMethod
    public Project ProjectUpdateByIndex(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "index") final Integer index,
            @WebParam(name = "name") final String name,
            @WebParam(name = "description") final String description
    ) {
        serviceLocator.getSessionService().validate(session);
        return projectService.updateByIndex(session.getUserId(), index, name, description);
    }

    @WebMethod
    public Project ProjectStartById(
            @WebParam(name = "session") final Session session, @WebParam(name = "id") final String id
    ) {
        serviceLocator.getSessionService().validate(session);
        return projectService.startById(session.getUserId(), id);
    }

    @WebMethod
    public Project ProjectStartByIndex(
            @WebParam(name = "session") final Session session, @WebParam(name = "index") final Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        return projectService.startByIndex(session.getUserId(), index);
    }

    @WebMethod
    public Project ProjectStartByName(
            @WebParam(name = "session") final Session session, @WebParam(name = "name") final String name
    ) {
        serviceLocator.getSessionService().validate(session);
        return projectService.startByName(session.getUserId(), name);
    }

    @WebMethod
    public Project ProjectFinishById(
            @WebParam(name = "session") final Session session, @WebParam(name = "id") final String id
    ) {
        serviceLocator.getSessionService().validate(session);
        return projectService.finishById(session.getUserId(), id);
    }

    @WebMethod
    public Project ProjectFinishByIndex(
            @WebParam(name = "session") final Session session, @WebParam(name = "index") final Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        return projectService.finishByIndex(session.getUserId(), index);
    }

    @WebMethod
    public Project ProjectFinishByName(
            @WebParam(name = "session") final Session session, @WebParam(name = "name") final String name
    ) {
        serviceLocator.getSessionService().validate(session);
        return projectService.finishByName(session.getUserId(), name);
    }
}

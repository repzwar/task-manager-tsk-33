package ru.pisarev.tm.component;

import org.jetbrains.annotations.NotNull;
import ru.pisarev.tm.bootstrap.Bootstrap;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static ru.pisarev.tm.constant.BackupConst.BACKUP_LOAD;
import static ru.pisarev.tm.constant.BackupConst.BACKUP_SAVE;

public class Backup {

    @NotNull
    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    @NotNull
    private static final int INTERVAL = 30;

    @NotNull
    private final Bootstrap bootstrap;

    public Backup(@NotNull Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void init() {
        load();
        es.scheduleWithFixedDelay(this::save, 0, INTERVAL, TimeUnit.SECONDS);
    }

    public void stop() {
        es.shutdown();
    }

    public void save() {
        bootstrap.runCommand(BACKUP_SAVE);
    }

    public void load() {
        bootstrap.runCommand(BACKUP_LOAD);
    }

}
